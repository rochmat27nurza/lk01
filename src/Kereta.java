public class Kereta {
    //Tulis kode disini
    private String namaKereta;
    private int kuotaTiket;
    private int penumpang;
    private Ticket[] jumlahTiket;
    
    // konstruktor kereta komuter
    public Kereta(){
        namaKereta = "Komuter";
        kuotaTiket = 1000;
        jumlahTiket = new Ticket[kuotaTiket];
    }

    // Konstruktor KAJJ
    public Kereta(String namaKereta, int kuotaTiket){
        this.namaKereta = namaKereta;
        this.kuotaTiket = kuotaTiket;
        jumlahTiket = new Ticket[this.kuotaTiket];
    }

    // method kereta komuter
    public void tambahTiket(String nama){
        System.out.println("===============================================");
        // pengecekan ketersediaan tiket dengan penumpang kereta komuter
        if (penumpang < kuotaTiket){
            Ticket ticketing = new Ticket(nama);
            jumlahTiket[penumpang] = ticketing;
            penumpang++;
            // Mencetak status pemesanan tiket
            // Kondisi untuk menampilkan perintah cetak jika tiket masih lebih dari 30
            if ( (kuotaTiket-penumpang) >= 30 ){
                System.out.println("Tiket berhasil dipesan");
            }
                // Kondisi untuk menampilkan perintah cetak jika tiket kurang dari 30
                else if((kuotaTiket-penumpang) <= 30){
                    System.out.println("Tiket berhasil dipesan. Sisa tiket tersedia: "+ (kuotaTiket-penumpang) );
                }
        }
        // output yang akan keluar jika tiket telah habis.
        else {
            System.out.println("===============================================");
            System.out.println("Tiket kereta telah habis dipesan. Silahkan cari jadwal keberangkatan lainnya.");
        }
        
    }
    
    // method KAJJ
    public void tambahTiket(String nama, String asal , String tujuan){
        System.out.println("===============================================");
        // pengecekan ketersediaan tiket dengan penumpang kereta Api Jarak Jauh
        if (penumpang < kuotaTiket){
            Ticket ticketing = new Ticket(nama, asal, tujuan);
            jumlahTiket[penumpang] = ticketing;
            penumpang++;
            // Mencetak status pemesanan tiket
            // Kondisi untuk menampilkan perintah cetak jika tiket masih lebih dari 30
            if ( (kuotaTiket-penumpang) >= 30 ){
                System.out.println("Tiket berhasil dipesan");
            }
                // Kondisi untuk menampilkan perintah cetak jika tiket kurang dari 30
                else if((kuotaTiket-penumpang) <= 30){
                    
                    System.out.println("Tiket berhasil dipesan. Sisa tiket tersedia: "+ (kuotaTiket-penumpang));
                }
        }
        // output yang akan keluar jika tiket telah habis.
        else {
            System.out.println("Tiket kereta telah habis dipesan.");
            System.out.println("Silahkan cari jadwal keberangkatan lainnya.");
        }
    }

    // menampilkan tiket yang dipesan oleh penumpang
    public void tampilkanTiket(){
        System.out.println("===============================================");
        System.out.println("Daftar penumpang kereta api "+ namaKereta + " :");
        System.out.println("-----------------------------------------------");
        for (int i = 0; i < penumpang; i++) {
            // Pengkondisian untuk mencetak tiket kereta sesuai jenisnya
            // KAJJ
            if (jumlahTiket[i].getAsal() != null){
                System.out.println("Nama \t: " + jumlahTiket[i].getNama());
                System.out.println("Asal \t: " + jumlahTiket[i].getAsal());
                System.out.println("Tujuan \t: " + jumlahTiket[i].getTujuan());
                System.out.println("-----------------------------------------------");
            }
            // Kereta komuter
            else {
                System.out.println("Nama \t: " + jumlahTiket[i].getNama());
            }  
        }
    }


}

